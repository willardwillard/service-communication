﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleProject.Requests.Models;
using Newtonsoft.Json;
using System.Net.Http;

namespace ConsoleProject.Requests
{
    class UserRequest : IRequest<UserModel>
    {
        private HttpClient client;
        public string URL { get; set; } = "https://localhost:44370/api/users";
        public UserRequest()
        {
            client = new HttpClient();

        }
        public List<UserModel> GetList()
        {
            var users = client.GetStringAsync($"{URL}");
            return JsonConvert.DeserializeObject<List<UserModel>>(users.Result);
        }
        public UserModel GetItem(int id)
        {
            var user = client.GetStringAsync($"{URL}/{id}");
            return JsonConvert.DeserializeObject<UserModel>(user.Result);
        }
        public string CreateItem(UserModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PostAsync($"{URL}", stringContent).Result;
            return result.ToString();
        }
        public string UpdateItem(int id, UserModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PutAsync($"{URL}/{id}", stringContent).Result;
            return result.ToString();
        }
        public string DeleteItem(int id)
        {
            var result = client.DeleteAsync($"{URL}/{id}").Result;
            return result.ToString();
        }
    }
      
}
