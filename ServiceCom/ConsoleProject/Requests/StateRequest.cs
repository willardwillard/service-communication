﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using ConsoleProject.Requests.Models;

namespace ConsoleProject.Requests
{
    class StateRequest
    {
        private HttpClient client;
        public string URL { get; set; } = "https://localhost:44370/api/states";
        public StateRequest()
        {
            client = new HttpClient();

        }
        public List<StateModel> GetList()
        {
            var states = client.GetStringAsync($"{URL}");
            return JsonConvert.DeserializeObject<List<StateModel>>(states.Result);
        }
        public StateModel GetItem(int id)
        {
            var state = client.GetStringAsync($"{URL}/{id}");
            return JsonConvert.DeserializeObject<StateModel>(state.Result);
        }
        public string CreateItem(StateModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PostAsync($"{URL}", stringContent).Result;
            return result.ToString();
        }
        public string UpdateItem(int id, StateModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PutAsync($"{URL}/{id}", stringContent).Result;
            return result.ToString();
        }
        public string DeleteItem(int id)
        {
            var result = client.DeleteAsync($"{URL}/{id}").Result;
            return result.ToString();
        }
    }
}
