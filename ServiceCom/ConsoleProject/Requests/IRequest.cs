﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject.Requests
{
    interface IRequest<T>
    {
        string URL { get; set; }
        List<T> GetList();
        T GetItem(int id);
        string CreateItem(T item);
        string UpdateItem(int id, T item);
        string DeleteItem(int id);
    }
}
