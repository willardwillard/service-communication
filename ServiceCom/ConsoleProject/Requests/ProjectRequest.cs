﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleProject.Requests.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace ConsoleProject.Requests
{
    class ProjectRequest : IRequest<ProjectModel>
    {
        private HttpClient client;
        public string URL { get; set; } = "https://localhost:44370/api/projects";
        public ProjectRequest()
        {
            client = new HttpClient();
            
        }
       public List<ProjectModel> GetList()
        {
            var projects = client.GetStringAsync($"{URL}");
            return JsonConvert.DeserializeObject<List<ProjectModel>>(projects.Result);
        }
        public ProjectModel GetItem(int id)
        {
            var project = client.GetStringAsync($"{URL}/{id}");
            return JsonConvert.DeserializeObject<ProjectModel>(project.Result);
        }
        public string CreateItem(ProjectModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PostAsync($"{URL}", stringContent).Result;
            return result.ToString();
        }
        public string UpdateItem(int id, ProjectModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PutAsync($"{URL}/{id}", stringContent).Result;
            return result.ToString();
        }
        public string DeleteItem(int id)
        {
            var result = client.DeleteAsync($"{URL}/{id}").Result;
            return result.ToString();
        }
    }
}
