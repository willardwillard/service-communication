﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleProject.Requests.Models;
using System.Net.Http;
using Newtonsoft.Json;


namespace ConsoleProject.Requests
{
    class TaskRequest : IRequest<TaskModel>
    {
        private HttpClient client;
        public string URL { get; set; } = "https://localhost:44370/api/tasks";
        public TaskRequest()
        {
            client = new HttpClient();

        }
        public List<TaskModel> GetList()
        {
            var tasks = client.GetStringAsync($"{URL}");
            return JsonConvert.DeserializeObject<List<TaskModel>>(tasks.Result);
        }
        public TaskModel GetItem(int id)
        {
            var tasks = client.GetStringAsync($"{URL}/{id}");
            return JsonConvert.DeserializeObject<TaskModel>(tasks.Result);
        }
        public string CreateItem(TaskModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PostAsync($"{URL}", stringContent).Result;
            return result.ToString();
        }
        public string UpdateItem(int id, TaskModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PutAsync($"{URL}/{id}", stringContent).Result;
            return result.ToString();
        }
        public string DeleteItem(int id)
        {
            var result = client.DeleteAsync($"{URL}/{id}").Result;
            return result.ToString();
        }
    }
}
