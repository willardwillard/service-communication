﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using ConsoleProject.Requests.Models;

namespace ConsoleProject.Requests
{
    class TeamRequest
    {
        private HttpClient client;
        public string URL { get; set; } = "https://localhost:44370/api/teams";
        public TeamRequest()
        {
            client = new HttpClient();

        }
        public List<TeamModel> GetList()
        {
            var teams = client.GetStringAsync($"{URL}");
            return JsonConvert.DeserializeObject<List<TeamModel>>(teams.Result);
        }
        public TeamModel GetItem(int id)
        {
            var team = client.GetStringAsync($"{URL}/{id}");
            return JsonConvert.DeserializeObject<TeamModel>(team.Result);
        }
        public string CreateItem(TeamModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PostAsync($"{URL}", stringContent).Result;
            return result.ToString();
        }
        public string UpdateItem(int id, TeamModel item)
        {
            var jsonObject = JsonConvert.SerializeObject(item);
            var stringContent = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            var result = client.PutAsync($"{URL}/{id}", stringContent).Result;
            return result.ToString();
        }
        public string DeleteItem(int id)
        {
            var result = client.DeleteAsync($"{URL}/{id}").Result;
            return result.ToString();
        }
    }
}
