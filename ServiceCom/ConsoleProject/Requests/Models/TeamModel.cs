﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ConsoleProject.Requests.Models
{
    class TeamModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
    }
}
