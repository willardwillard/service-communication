﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ConsoleProject.Requests.Models
{
    class StateModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
