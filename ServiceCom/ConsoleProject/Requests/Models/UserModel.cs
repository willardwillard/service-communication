﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ConsoleProject.Requests.Models
{
    class UserModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("birthday")]
        public DateTime Birthday { get; set; }
        [JsonProperty("registered_at")]
        public DateTime RegisteredAt { get; set; }
        [JsonProperty("team_id")]
        public int? TeamId { get; set; }
    }
}
