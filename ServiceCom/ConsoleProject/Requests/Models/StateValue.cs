﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleProject.Requests.Models
{
    enum StateValue
    {
        Created = 0,
        Started,
        Finished,
        Canceled
    }
}
